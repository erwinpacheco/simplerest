from django.contrib import admin
from django.contrib.auth import authenticate
from django.urls import include, path

from django.contrib.auth.models import User, Group
admin.autodiscover()

from rest_framework import generics, permissions, serializers, decorators
from rest_framework.authtoken.models import Token

from oauth2_provider.contrib.rest_framework import TokenHasReadWriteScope, TokenHasScope
from oauth2_provider.models import AccessToken
from django.http import JsonResponse

import json

from django.core.mail import send_mail, BadHeaderError

class UserSerializer(serializers.ModelSerializer):
    def create(self, validated_data):

        if not 'email' in validated_data:
            raise serializers.ValidationError('requires email')
        user = User.objects.create_user(**validated_data, is_active = False)
        token = Token.objects.create(user=user)

        # Send token thru email
        try:
            send_mail(
                'Email verification',
                'Your activation token is %s ' % token,
                'no@reply',
                [validated_data["email"]],
                fail_silently=False,
            )
        except BadHeaderError:
            print("Bad Header")

        return user

    class Meta:
        model = User
        fields = ('username', 'email', "first_name", "last_name", "password")

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ("name", )

# Create the API views
class UserDetails(generics.RetrieveAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasReadWriteScope]
    queryset = User.objects.all()
    serializer_class = UserSerializer

class GroupList(generics.ListAPIView):
    permission_classes = [permissions.IsAuthenticated, TokenHasScope]
    required_scopes = ['groups']
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

@decorators.api_view(['POST','GET','PATCH'])
@decorators.permission_classes([permissions.AllowAny,])
def Users(request):
    data = request.data
    if request.method == 'POST':
        # Activation endpoint
        if 'token' in data:
            username = data["username"]
            try:
                if not str(Token.objects.get(key=Token(data["token"])).user) == username:
                    details = "Token %s does not match user %s." % (data["token"], username)
                    return JsonResponse({"detaiis": details})
                else:    
                    user = User.objects.get(username = username)
                    user.is_active = True
                    user.save()
                    serializer = UserSerializer(user, partial=True)
                    print(serializer.data)
                    serializer_data = serializer.data
                    del serializer_data["password"]
                    return JsonResponse("%s" % serializer_data, safe=False)

            except Exception as e:
                return JsonResponse({"detaiis": "%s" % e})

        # Registration endpoint
        else:
            serializer = UserSerializer(data=data, partial=True)
            if serializer.is_valid():
                if not 'email' in serializer.validated_data:
                    raise serializers.ValidationError('requires email')
                try:
                    try:
                        serializer.save(
                            username=serializer.validated_data['username'],
                            email=serializer.validated_data['email'],
                            password=serializer.validated_data['password'],
                            first_name=serializer.validated_data['first_name'],
                            last_name=serializer.validated_data['last_name'],
                        )
                    except:
                        serializer.save(
                            username=serializer.validated_data['username'],
                            email=serializer.validated_data['email'],
                            password=serializer.validated_data['password'],
                        )
                except KeyError as e:
                    details = "Key error: %s" % e
                    return JsonResponse(details, safe=False)

                details = "Check email for token"
                return JsonResponse({"detaiis": details})
            else:
                return JsonResponse(serializer.errors)#, status=status.HTTP_400_BAD_REQUEST)
    elif request.method == 'GET':
        # UserList endpoint
        serializer = UserSerializer(User.objects.all(), many=True)
        for d in serializer.data:
            del d["password"]

        if request.META.get("HTTP_AUTHORIZATION", "").startswith("Bearer"):
            b,token = (request.META.get("HTTP_AUTHORIZATION")).split(" ")
            try:
                AccessToken.objects.get(token=token)
            except Exception as e:
                return JsonResponse({"detaiis": '%s' % e })
        else:
            for d in serializer.data:
                del d["email"]
                del d["last_name"]

        return JsonResponse(serializer.data, safe=False)
    else: #request.method == 'PATCH'
        if request.META.get("HTTP_AUTHORIZATION", "").startswith("Bearer"):
            b,token = (request.META.get("HTTP_AUTHORIZATION")).split(" ")
        else:
            details = "Invalid credentials given."
            return JsonResponse({"detaiis": details})
        username = AccessToken.objects.get(token=token).user
        user = authenticate(request, username=username, password=data['password'])
        if user is None:
            details = "Invalid credentials given."
            return JsonResponse({"detaiis": details})
        user.set_password(data["new_password"])
        user.save()
        details = "Password change successful."
        return JsonResponse({"detaiis": details})#, status=status.HTTP_200_OK)

# Setup the URLs and include login URLs for the browsable API.
urlpatterns = [
    path('admin/', admin.site.urls),
    path('o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    path('users/', Users),
    path('users/<pk>/', UserDetails.as_view()),
    path('groups/', GroupList.as_view()),
]



