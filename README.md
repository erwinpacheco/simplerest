**A Simple RESTful web service.**


---

## Setup


### settings.py has following lines:


	INSTALLED_APPS = [
		...
    	'rest_framework',
	    'rest_framework.authtoken',
    	'oauth2_provider',
		...
	]
	
	OAUTH2_PROVIDER = {
    'SCOPES': {'read': 'Read scope', 'write': 'Write scope', 'groups': 'Access to your groups'}
	}

	REST_FRAMEWORK = {
    	'DEFAULT_AUTHENTICATION_CLASSES': [
        	'oauth2_provider.contrib.rest_framework.OAuth2Authentication',
	        'rest_framework.authentication.BasicAuthentication',
		],
    	'DEFAULT_PERMISSION_CLASSES': (
        	'rest_framework.permissions.IsAuthenticated',
    	)
	}


### Register an application

Name					: (any name)			
Client id				: (provided)
Client secret			: (provided)
Client type 			: Confidential
Authorization grant type: Resource owner password-based
Redirect uris			: (blank)

#### Provide 'Client id' and 'Client secret' as client application's auth parameters


---

## Usage

### User Registration 


e.g.1 

	curl -X POST -H "Content-Type: application/json" -d '{"username":"test1", "password":"testtest11", "email":"test1@testers.com"}' http://localhost:8000/users/

e.g.2 opts to include first_name and last_name fields

	curl -X POST -H "Content-Type: application/json" -d '{"username":"test1", "password":"testtest11", "email":"test1@testers.com", "first_name":"test1", "last_name":"tester"}' http://localhost:8000/users/

#### Registration sends activation token thru email


### User Activation


e.g.

	curl -H "Content-Type: application/json" -X POST -d '{"username":"test1", "token":"6bac1fe472ca20718daaa5b824433e451450f892"}' http://localhost:8000/users/

#### Activation sends serialized user details excluding the password hash 


### Login


e.g.

	curl -X POST -d "grant_type=password&username=test25&password=testtest2525" -u"ZiA0of461fFKpB5GWKxeb8PGve4y11bBWI30VQkp:YEvTe4Pb62Ioq2YlPShgXahrIRuoI2leY1BwZ26uWYddJJUaaLQ9rNRMs2L4idnQN7mRNE9M2Knv6jTDrMDBni4wVKwjYfGqCJEwF7gH13EDqzAklEWS5MgoN9jA3Ii3" http://localhost:8000/o/token/

#### Login sends OAuth2 bearer tokens


### User List


e.g.1 show list of user fields 'username' and 'first_name' 

	curl http://localhost:8000/users/

e.g.2 show list of user fields 'username', 'first_name', 'last_name', 'email address'

	curl -H "Authorization: Bearer GAO5EPukkvqF858PChaNeykvZzBsW9"  http://localhost:8000/users/

#### User List without OAuth2 bearer access_token has limited view of user fields.


### Change password


e.g.

	curl -X PATCH -d "password=testtest25258&new_password=justtest25" -H "Authorization: Bearer GAO5EPukkvqF858PChaNeykvZzBsW9"  http://localhost:8000/users/

#### 

---
## Preferences

Python and python packages versions to be used on the development.

1.	Python 3.7.3
2.	Django 2.2.6
3.	djangorestframework-3.10.3
4.	django-oauth-toolkit-1.2.0

*Use pip3 to include package requirements on installation.*

---

## Status

Users listing responds thru url:  /users/
*e.g. running on local http://127.0.0.1:8000/users/*
Refactor codes from files view.py and serializers.py to urls.py
Applied OAuth2 and BasicAuth as default authentication classes


Following commits order: endpoint[ChangePassword], endpoint[UserList] with OAuth2 provided